# Licensing policy

## Tracks

### Software logos

* [Android : CC BY 3.0](https://developer.android.com/distribute/tools/promote/brand.html)
* [Debian : LGPLv3+ and CC BY-SA 3.0](https://www.debian.org/logos/)
* [GNU Emacs : GPLv3.0+](https://commons.wikimedia.org/wiki/File:EmacsIcon.svg)
* [GNU Octave : GPLv2.0+](https://commons.wikimedia.org/wiki/File:Gnu-octave-logo.svg)
* [GNU R : GPLv2.0+ and CC BY-SA](http://developer.r-project.org/Logo/README)
* [Transmission : Expat](https://fr.wikipedia.org/wiki/Fichier:Transmission_icon.png)
* [VLC : GPLv2.0+](https://fr.wikipedia.org/w/index.php?title=Fichier:VLC_Icon.svg)

### Tracks of video games

If this tracks are not in public domain, there are probably under a non free/libre license.
But in some countries, there is "fair use".

#### Mario Kart

Tracks of proprietary Mario Kart games are probably under Nitendo copyright in some countries.

#### F-Zero

Tracks of proprietary F-Zero games are probably under Nitendo copyright in some countries.

### Others

Others tracks are under [unlicense](LICENSE_UNLICENSE) that is a public domain license.

## Source code

If there is no header that defines a license, you should consider that the license is [GNU LGPL](https://www.gnu.org/licenses/lgpl.html) version 3 (or at your option any later version).

## Documentation

If there is no information about the license in a header of a source file of documentation, it is under [GPL](https://www.gnu.org/licenses/gpl.html)v3.0+, [GFDL](https://www.gnu.org/copyleft/fdl.html)v1.3, [Creative Commons BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) 4.0 and [FAL](http://artlibre.org/licence/lal/)v1.3.
You can choose the license you wish among those mentioned.
It is also true for the version if multiple versions are proposed for a given license.

Markdown and LaTeX files are covered by this section.
