#!/usr/bin/env python2
# coding: utf-8


# The starters of this project (see git commits in 2014) published their work under unlicense (<http://unlicense.org/>).
# Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from __future__ import print_function
import os
import sys
import re
from math import ceil
from PIL import Image


ELEMENTS = (
    ('#', (255, 255, 255)),
    ('.', (0,     0,   0)),
    ('~', (255, 255,   0)),
    ('=', (  0, 255,   0)),
    ('1', (  0,   0, 255)),
    ('2', (  0, 255, 255)),
    ('3', (255,   0, 255)),
)


def color_to_formated_str(color):
    str = ''
    if len(color) > 0:
        str += format(color[0], "3d")
        i = 1
        while i < len(color):
            str += ',' + format(color[i], "3d")
            i += 1
    return '['+ str +']'

def position_to_str(x, y, x_max, y_max):
    return '(x='+ str(x) +'/'+ str(x_max) +', y='+ str(y) +'/'+ str(y_max) +')'

def convert(srcfile, dstfile, defineFuel=True):
    src = Image.open(srcfile)
    dst = open(dstfile, 'w')

    dst.write(str(src.size[0]) + ' ' + str(src.size[1]))
    if defineFuel:
        dst.write(' '+ str(ceil(src.size[0] * src.size[1] * 0.08)))
    dst.write('\n')
    for y in range(src.size[1]):
        for x in range(src.size[0]):
            c = src.getpixel((x, y))
            
            if len(c) == 4:
                print('The alpha channel must be removed.',
                      file=sys.stderr)
                os.remove(dstfile)
                sys.exit(1)
            
            print_px_tolerated = True
            for i in range(len(c)):
                if c[i] != 0 and c[i] < 55:
                    if print_px_tolerated:
                        print(
                            'Pixel tolerated '+
                            position_to_str(x, y, src.size[0], src.size[1]) +
                            ' ' + color_to_formated_str(c),
                            file=sys.stderr
                        )
                        print_px_tolerated = False
                    c = list(c)
                    c[i] = 0
                elif c[i] != 255 and c[i] > 200:
                    if print_px_tolerated:
                        print(
                            'Pixel tolerated '+
                            position_to_str(x, y, src.size[0], src.size[1]) +
                            ' '+ color_to_formated_str(c),
                            file=sys.stderr
                        )
                        print_px_tolerated = False
                    c = list(c)
                    c[i] = 255
            if not print_px_tolerated:
                c = tuple(c)
            
            for char, color in ELEMENTS:
                if color == c:
                    dst.write(char)
                    break
            else:
                print(
                    'Unknown pixel '+
                    position_to_str(x, y, src.size[0], src.size[1]) +
                    ' ' + color_to_formated_str(c),
                    file=sys.stderr
                )
                os.remove(dstfile)
                sys.exit(1)
        dst.write('\n')
    dst.close()


def convert_back(srcfile, dstfile):
    src = open(srcfile, 'r')

    sizeline = src.readline()
    regex = re.compile('\d+')
    size = regex.findall(sizeline)
    if len(size) != 2:
        print(
            'Invalid size', sizeline,
            file=sys.stderr
        )
        sys.exit(1)
    width  = int(size[0])
    height = int(size[1])

    dst = Image.new('RGB', (width, height))

    for y in range(height):
        for x in range(width):
            c = src.read(1)
            for char, color in ELEMENTS:
                if char == c:
                    dst.putpixel((x, y), color)
                    break
            else:
                print('Unknown character', c, file=sys.stderr)
                sys.exit(1)
        src.read(1)  # \n

    dst.save(dstfile)
