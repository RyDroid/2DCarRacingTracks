# Collection of tracks for 2D car racing simulations

It is mostly a [free/libre](https://www.gnu.org/philosophy/free-sw.html) project (see [the license file](LICENSE.md)).
Feel free to contribute!

There is at least [one free/libre simulator that supports this tracks](https://gitlab.com/RyDroid/Libre2DCarRacingsSimulator).

## Images

* The converter function does not understand index color mode.
* The converter does not understand the alpha channel.
  You can easily [remove it with GIMP](http://docs.gimp.org/en/gimp-layer-alpha-remove.html).

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md)
