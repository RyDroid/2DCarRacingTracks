#!/usr/bin/env python2
# coding: utf-8


# The starters of this project (see git commits in 2014) published
# their work under unlicense (<http://unlicense.org/>).
#
# Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from __future__ import print_function
from converter_functions import *
import sys


def is_collection_containing_help_option_str(collection):
    return (
        "-?"       in collection or
        "-h"       in collection or
        "--help"   in collection or
        "--manual" in collection or
        "--info"   in collection
    )

if __name__ == '__main__':
    if len(sys.argv) < 3 or is_collection_containing_help_option_str(sys.argv):
        print("Usage: "+ str(sys.argv[0]) +" input_file_path output_file_path",
              file=sys.stderr)
        sys.exit(1)
    
    input_file_path  = sys.argv[len(sys.argv) - 2];
    output_file_path = sys.argv[len(sys.argv) - 1];
    if input_file_path.endswith('.png') or input_file_path.endswith('.PNG'):
        if '--carburant' in sys.argv or '--fuel' in sys.argv or '--fuel=true' in sys.argv:
            convert(input_file_path, output_file_path, True)
        elif '--no-carburant' in sys.argv or '--no-fuel' in sys.argv or '--fuel=false' in sys.argv:
            convert(input_file_path, output_file_path, False)
        else:
            convert(input_file_path, output_file_path)
    elif input_file_path.endswith('.txt') or input_file_path.endswith('.TXT'):
        convert_back(input_file_path, output_file_path)
    else:
        print("The input file must be a *.txt or *.png",
              file=sys.stderr)
        sys.exit(1)
