# Contributing

## Naming conventions

### Naming conventions for tracks

A track file name should be fully in [ASCII](https://en.wikipedia.org/wiki/ASCII).
Thanks to ASCII, tracks are easy to manage through a terminal and there is no problem to manage them with a 8bit type for a character (like char in C and libc functions for printing that do not manage UTF8 or UTF16).

A track file name should be fully in lower case.
Thanks to lower case, tracks are easy to manage through a terminal and there is no potential collision with non sensitive case file systems.
Abbreviations could be an exception to lower case.

A track file name should follow this pattern: `type_name_potential-variant-name`.
Generic types are: starter, test, software and real.
However, you - of course - should use an other one if it is appropriate.

## Things to do

* Improve the speed of the converter
 * Checking tolerance can be done with threads (by divinding image width or height by the number of cores).
 * PIL for Python can convert images in [PPM (Portable PixMap)](https://en.wikipedia.org/wiki/Netpbm_format).
   Converting in PNG to PPM with Python and then checking tolerance and converting in txt with an implementation in an other language closest to the machine (like C, C++, Go or Rust) may be more efficient.
   PPM has the advantage to be easier than PNG to parse.
 * Rewriting it fully with a language closest to the machine (like C, C++, Go or Rust).
   It has been done in [Libre2DCarRacingsSimulator](https://gitlab.com/RyDroid/Libre2DCarRacingsSimulator) with a binary called map-2d-converter.
