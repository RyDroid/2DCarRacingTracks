# Copyright (C) 2015  Nicola Spanti (RyDroid) <rydroid_dev@yahoo.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


CONVERTER=./converter.py
RM=rm -f

TXT_TO_CONVERT=$(wildcard drop_to_convert/*.txt)
PNG_TO_CREATE=$(patsubst drop_to_convert/%,%,$(TXT_TO_CONVERT:.txt=.png))

PNGS=$(wildcard *.png) $(PNG_TO_CREATE)
MAPS=$(PNGS:.png=.txt)

PACKAGE=2DCarRacingTracks
FILES_TO_ARCHIVE=$(MAPS) *.py makefile *.md LICENSE_* \
	.editorconfig .gitignore .gitlab-ci.yml .travis.yml


all: $(MAPS) $(PNG_TO_CREATE)

%.txt: %.png
	@chmod +x $(CONVERTER)
	$(CONVERTER) $< $@

%.png: drop_to_convert/%.txt
	@chmod +x $(CONVERTER)
	$(CONVERTER) $< $@


archives: archives-tar zip 7z

archives-tar: tar-gz tar-bz2 tar-xz

default-archive: tar-xz

zip: $(PACKAGE).zip

$(PACKAGE).zip: $(FILES_TO_ARCHIVE)
	zip $(PACKAGE).zip -r -- $(FILES_TO_ARCHIVE) > /dev/null

tar-gz: $(PACKAGE).tar.gz

$(PACKAGE).tar.gz: $(FILES_TO_ARCHIVE)
	tar -zcvf $(PACKAGE).tar.gz -- $(FILES_TO_ARCHIVE) > /dev/null

tar-bz2: $(PACKAGE).tar.bz2

$(PACKAGE).tar.bz2: $(FILES_TO_ARCHIVE)
	tar -jcvf $(PACKAGE).tar.bz2 -- $(FILES_TO_ARCHIVE) > /dev/null

tar-xz: $(PACKAGE).tar.xz

$(PACKAGE).tar.xz: $(FILES_TO_ARCHIVE)
	tar -cJvf $(PACKAGE).tar.xz -- $(FILES_TO_ARCHIVE) > /dev/null

7z: $(PACKAGE).7z

$(PACKAGE).7z: $(FILES_TO_ARCHIVE)
	7z a -t7z $(PACKAGE).7z $(FILES_TO_ARCHIVE) > /dev/null


clean: clean-bin clean-tmp clean-archives clean-latex clean-maps
	$(RM) -rf -- \
		*.pyc __pycache__ *.elc \
		.*rc .bash* .zsh* .R* .octave* .xsession*

clean-bin:
	$(RM) -rf -- \
		*.o *.a *.so *.ko *.dll *.out \

clean-tmp:
	$(RM) -rf -- \
		*~ .\#* \#* *.bak *.save *.autosave \
		*.log *.log.* \
		.cache/ .thumbnails/

clean-archives:
	$(RM) -rf -- \
		*.deb *.rpm *.exe *.msi *.dmg *.apk \
		*.zip *.tar.* *.tgz *.7z *.gz *.bz2 *.lz *.lzma *.xz

clean-latex:
	$(RM) -rf -- \
		*.pdf *.dvi \
		*.aux *.nav *.toc *.fls *.snm *.vrb *.vrm *.fdb_latexmk *.synctex *.synctex.gz *-converted-to.*

clean-maps:
	$(RM) -- $(MAPS)

clean-proprietary:
	$(RM) -- mario_* Mario_* f-zero_* f-Zero_*

clean-git:
	git clean -fdx
